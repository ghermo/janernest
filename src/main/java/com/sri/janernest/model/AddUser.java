package com.sri.janernest.model;


import java.io.Serializable;

/**
 * Created by JMXPSX on 4/7/14.
 */
public class AddUser implements Serializable{

    private String userType;
    private String company;
    private String name;
    private String userName;
    private String password;
    private String title;
    private String emailAddress;
    private String contactNumber;
    private String status;

    public AddUser(String userType, String company, String name, String userName, String password, String title, String emailAddress, String contactNumber, String status) {
        this.userType = userType;
        this.company = company;
        this.name = name;
        this.userName = userName;
        this.password = password;
        this.title = title;
        this.emailAddress = emailAddress;
        this.contactNumber = contactNumber;
        this.status = status;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
