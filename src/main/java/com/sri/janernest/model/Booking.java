package com.sri.janernest.model;

/**
 * Created by grg021 on 3/28/14.
 */

import java.io.Serializable;
import java.util.Date;

public class Booking implements Serializable {

    private int bookingid;
    private Date bookingDate;
    private int bookingNumber;
    private String bookingCompany;
    private String origin;
    private String destination;
    private String status;

    public Booking(int bookingid, Date bookingDate, int bookingNumber, String bookingCompany, String origin, String destination, String status) {
        this.bookingid = bookingid;
        this.bookingDate = bookingDate;
        this.bookingNumber = bookingNumber;
        this.bookingCompany = bookingCompany;
        this.origin = origin;
        this.destination = destination;
        this.status = status;
    }

    public int getBookingid() {
        return bookingid;
    }

    public void setBookingid(int bookingid) {
        this.bookingid = bookingid;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public int getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(int bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getBookingCompany() {
        return bookingCompany;
    }

    public void setBookingCompany(String bookingCompany) {
        this.bookingCompany = bookingCompany;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
