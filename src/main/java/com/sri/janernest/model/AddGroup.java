package com.sri.janernest.model;

/**
 * Created by Jimmy on 4/1/14.
 */

import java.io.Serializable;


public class AddGroup implements Serializable{



    private String description;
    private String group_name;
    private String group_code;


        public AddGroup(String group_code, String group_name, String description){

            this.description = description;
            this.group_code = group_code;
            this.group_name = group_name;

            }


    public String getdescription() {
        return description;
    }

    public void setdescription(int no_of_Members) {
        description = description;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }
}


