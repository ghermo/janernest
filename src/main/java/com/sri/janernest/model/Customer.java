package com.sri.janernest.model;

import java.io.Serializable;

/**
 * Created by Clarence Victoria on 4/1/14.
 */
public class Customer implements Serializable{

    private int c_id;
    private String c_code;
    private String c_name;
    private String c_email;
    private String c_contact;

    public int getC_id() {
        return c_id;
    }

    public void setC_id(int c_id) {
        this.c_id = c_id;
    }

    public String getC_code() {
        return c_code;
    }

    public void setC_code(String c_code) {
        this.c_code = c_code;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getC_email() {
        return c_email;
    }

    public void setC_email(String c_email) {
        this.c_email = c_email;
    }

    public String getC_contact() {
        return c_contact;
    }

    public void setC_contact(String c_contact) {
        this.c_contact = c_contact;
    }

    public Customer(int c_id, String c_code, String c_name, String c_email, String c_contact) {

        this.c_id = c_id;
        this.c_code = c_code;
        this.c_name = c_name;
        this.c_email = c_email;
        this.c_contact = c_contact;
    }
}
