package com.sri.janernest.model;

/**
 * Created by Solutions Resource on 3/27/14.
 */
public class CustomerStore {


    private String customer;

    public CustomerStore(){
        setCustomer("Hi Im Customer");
    }

    public String getCustomer() {return customer;}

    public void setCustomer(String customer) {this.customer = customer;}

}
