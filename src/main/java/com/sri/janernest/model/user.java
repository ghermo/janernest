package com.sri.janernest.model;

/**
 * Created by JMXPSX on 3/27/14.
 */
public class user {

    private int userCode;
    private String name;
    private String companyName;
    private String userName;
    private String password;
    private String title;
    private String emailAddress;
    private String contactNumber;
    private String userType;
    private String status;

    public user(int userCode, String name, String companyName, String userName, String password, String title, String emailAddress, String contactNumber, String userType, String status) {
        this.userCode = userCode;
        this.name = name;
        this.companyName = companyName;
        this.userName = userName;
        this.password = password;
        this.title = title;
        this.emailAddress = emailAddress;
        this.contactNumber = contactNumber;
        this.userType = userType;
        this.status = status;
    }

    public int getUserCode() {
        return userCode;
    }

    public void setUserCode(int userCode) {
        this.userCode = userCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
