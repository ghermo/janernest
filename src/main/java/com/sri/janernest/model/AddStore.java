package com.sri.janernest.model;

/**
 * Created by papa on 3/27/14.
 */
public class AddStore {

    private String message;

    public AddStore() {

        setMessage("Hello, Lets Add a user!");
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }
}
