package com.sri.janernest.model;

/**
 * Created by Jimmy on 4/1/14.
 */

import java.io.Serializable;


public class Help implements Serializable{



    private int No_of_Members;
    private String group_name;
    private String group_code;


        public Help(String group_code, String group_name, int No_of_Members){

            this.No_of_Members = No_of_Members;
            this.group_code = group_code;
            this.group_name = group_name;

            }


    public int getNo_of_Members() {
        return No_of_Members;
    }

    public void setNo_of_Members(int no_of_Members) {
        No_of_Members = no_of_Members;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }
}


