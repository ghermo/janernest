package com.sri.janernest.model;

import java.io.Serializable;

/**
 * Created by grg021 on 3/27/14.
 */
public class vendor implements Serializable{

    private int v_id;
    private String v_code;
    private String v_name;
    private String v_type;
    private String v_class;

    public vendor(int v_id, String v_code, String v_name, String v_type, String v_class) {
        this.v_id = v_id;
        this.v_code = v_code;
        this.v_name = v_name;
        this.v_type = v_type;
        this.v_class = v_class;
    }

    public int getV_id() {
        return v_id;
    }

    public void setV_id(int v_id) {
        this.v_id = v_id;
    }

    public String getV_code() {
        return v_code;
    }

    public void setV_code(String v_code) {
        this.v_code = v_code;
    }

    public String getV_name() {
        return v_name;
    }

    public void setV_name(String v_name) {
        this.v_name = v_name;
    }

    public String getV_type() {
        return v_type;
    }

    public void setV_type(String v_type) {
        this.v_type = v_type;
    }

    public String getV_class() {
        return v_class;
    }

    public void setV_class(String v_class) {
        this.v_class = v_class;
    }
}
