package com.sri.janernest.model;

/**
 * Created by grg021 on 3/27/14.
 */
public class MessageStore {

    private String message;

    public MessageStore() {

        setMessage("Hello, Lets Greg Darla user");
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }

}
