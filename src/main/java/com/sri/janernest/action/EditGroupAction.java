package com.sri.janernest.action;

/**
 * Created by Jimmy on 4/1/14.
 */


import com.opensymphony.xwork2.ActionSupport;

public class EditGroupAction extends ActionSupport {

    private static final long serialVersionUID = 1L;

    public String name;
    public String code;
    public String desc;

          public String execute() throws Exception{


            return SUCCESS;

          }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
