package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Created by JMXPSX on 4/23/14.
 */
public class LoginAction extends ActionSupport{
    private String username;
    private String password;

    public String execute() throws Exception {

        return "success";
    }

    public void validate() {
        if (getUsername().length() == 0) {
            addFieldError("username", "User Name is required");
        }

        if (getPassword().length() == 0) {
            addFieldError("password", getText("password.required"));
        }

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
