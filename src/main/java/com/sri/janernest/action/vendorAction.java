package com.sri.janernest.action;

/**
 * Created by Clarence Victoria on 3/27/14.
 */

import com.sri.janernest.model.vendor;
import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

public class vendorAction extends ActionSupport {

    private static final long serialVersionUID = 1L;

    private List<vendor> vendorList;
    private List<String> searchByList;
    private List<String> typeList;
    private List<String> vendorTypeList;
    private List<String> truckTypeList;
    private List<String> statusList;
    private List<String> otherInfoClassList;

    private vendor vendor1;
    private vendor vendor2;
    private vendor vendor3;
    private vendor vendor4;
    private vendor vendor5;
    private vendor vendor6;
    private vendor vendor7;
    private vendor vendor8;
    private vendor vendor9;
    private vendor vendor10;



    public String execute() throws Exception{
        vendorList = new ArrayList<vendor>();

        vendorTypeList = new ArrayList<String>();
        vendorTypeList.add("---");
        vendorTypeList.add("Trucking");
        vendorTypeList.add("Shipping");

        vendor1 = new vendor(1, "SRI", "Solutions Resource", "Trucking", "Premium");
        vendor2 = new vendor(2, "SRI", "Solutions Resource", "Trucking", "Premium");
        vendor3 = new vendor(3, "SRI", "Solutions Resource", "Trucking", "Premium");
        vendor4 = new vendor(4, "SRI", "Solutions Resource", "Trucking", "Premium");
        vendor5 = new vendor(5, "SRI", "Solutions Resource", "Trucking", "Premium");
        vendor6 = new vendor(6, "SRI", "Solutions Resource", "Trucking", "Premium");
        vendor7 = new vendor(7, "SRI", "Solutions Resource", "Trucking", "Premium");
        vendor8 = new vendor(8, "SRI", "Solutions Resource", "Trucking", "Premium");
        vendor9 = new vendor(9, "SRI", "Solutions Resource", "Trucking", "Premium");
        vendor10 = new vendor(10, "SRI", "Solutions Resource", "Trucking", "Premium");

        vendorList.add(vendor1);
        vendorList.add(vendor2);
        vendorList.add(vendor3);
        vendorList.add(vendor4);
        vendorList.add(vendor5);
        vendorList.add(vendor6);
        vendorList.add(vendor7);
        vendorList.add(vendor8);
        vendorList.add(vendor9);
        vendorList.add(vendor10);

        searchByList = new ArrayList<String>();
        searchByList.add("Company Name");
        searchByList.add("Company Code");
        searchByList.add("Vendor Type");
        searchByList.add("Email Address");
        searchByList.add("Contact Number");
        searchByList.add("Class");
        searchByList.add("City");

        typeList = new ArrayList<String>();
        typeList.add("Main");
        typeList.add("Home");
        typeList.add("Branch 1");
        typeList.add("Branch 2");
        typeList.add("Branch 3");
        typeList.add("Annex 1");
        typeList.add("Annex 2");
        typeList.add("Annex 3");

        truckTypeList = new ArrayList<String>();
        truckTypeList.add("Tractor Head");
        truckTypeList.add("Closed Van");

        statusList = new ArrayList<String>();
        statusList.add("Active");
        statusList.add("Inactive");

        otherInfoClassList = new ArrayList<String>();
        otherInfoClassList.add("---");
        otherInfoClassList.add("Premium");
        otherInfoClassList.add("Class A");
        otherInfoClassList.add("Class B");
        otherInfoClassList.add("Class C");
        otherInfoClassList.add("Economy");

        return SUCCESS;
    }

    public List<String> getSearchByList() {
        return searchByList;
    }

    public void setSearchByList(List<String> searchByList) {
        this.searchByList = searchByList;
    }

    public List<vendor> getVendorList() {
        return vendorList;
    }

    public void setVendorList(List<vendor> vendorList) {
        this.vendorList = vendorList;
    }

    public List<String> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<String> typeList) {
        this.typeList = typeList;
    }

    public List<String> getVendorTypeList() {
        return vendorTypeList;
    }

    public void setVendorTypeList(List<String> vendorTypeList) {
        this.vendorTypeList = vendorTypeList;
    }

    public List<String> getTruckTypeList() {
        return truckTypeList;
    }

    public void setTruckTypeList(List<String> truckTypeList) {
        this.truckTypeList = truckTypeList;
    }

    public List<String> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<String> statusList) {
        this.statusList = statusList;
    }

    public List<String> getOtherInfoClassList() {
        return otherInfoClassList;
    }

    public void setOtherInfoClassList(List<String> otherInfoClassList) {
        this.otherInfoClassList = otherInfoClassList;
    }


}
