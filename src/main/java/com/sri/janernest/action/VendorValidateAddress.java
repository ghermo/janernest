package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clarence Victoria on 4/16/14.
 */
public class VendorValidateAddress extends ActionSupport {

    private String street;
    private String type;
    private String city;
    private String zip;

    private List<String> typeList;

    public void validate(){
        if (street == null || street.trim().equals("")){
            addFieldError("street", "Null");
        }
        if (type == null || type.trim().equals("")){
            addFieldError("type", "Null");
        }
        if (city == null || city.trim().equals("")){
            addFieldError("city", "Null");
        }
        if (zip == null || zip.trim().equals("")){
            addFieldError("zip", "Null");
        }

        typeList = new ArrayList<String>();
        typeList.add("Main");
        typeList.add("Home");
        typeList.add("Branch 1");
        typeList.add("Branch 2");
        typeList.add("Branch 3");
        typeList.add("Annex 1");
        typeList.add("Annex 2");
        typeList.add("Annex 3");
    }

    public String execute(){

        return SUCCESS;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public List<String> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<String> typeList) {
        this.typeList = typeList;
    }
}
