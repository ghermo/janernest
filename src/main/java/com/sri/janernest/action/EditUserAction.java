package com.sri.janernest.action;

/**
 * Created by ADMIN on 4/3/14.
 */
import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

public class EditUserAction extends ActionSupport {

    public List<String> userType;
    public String company;
    public String name;
    public String username;
    public String password;
    public String title;
    public String email;
    public String contact;
    public String status;


    public String execute()
    {

        return SUCCESS;
    }


    public EditUserAction(){

        userType = new ArrayList<String>();
        userType.add("Registered User");
        userType.add("Customer Relations Department");
        userType.add("Freight Operations Officer");
        userType.add("Freight Operations Specialist");
        userType.add("Freight Operations Manager");
        userType.add("Freight Documents Specialist");
        userType.add("Inland Freight Officer");

    }

    public void validate() {
        if(company == null || company.trim().equals("")){
            addFieldError("company","The code is required");
        }
        if(name == null || name.trim().equals("")){
            addFieldError("name","The name is required" + name);
        }
        if(username == null || username.trim().equals("")){
            addFieldError("username","The Description is required");
        }
        if(password == null || password.trim().equals("")){
            addFieldError("password","The code is required");
        }
        if(title == null || title.trim().equals("")){
            addFieldError("title","The name is required" + name);
        }
        if(email == null || email.trim().equals("")){
            addFieldError("email","The name is required" + name);
        }
        if(contact == null || contact.trim().equals("")){
            addFieldError("contact","The code is required");
        }
        if(status == null || status.trim().equals("")){
            addFieldError("status","The name is required" + name);
        }



    }

    public List<String> getUserType() {
        return userType;
    }

    public void setUserType(List<String> userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}