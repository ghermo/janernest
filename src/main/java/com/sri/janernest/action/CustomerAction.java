package com.sri.janernest.action;

/**
 * Created by Solutions Resource on 3/27/14.
 */

import com.sri.janernest.model.Customer;
import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

public class CustomerAction extends ActionSupport {

    private static final long serialVersionUID = 1L;

    public List<String> getCustomerSearch() {
        return customerSearch;
    }

    public void setCustomerSearch(List<String> customerSearch) {
        this.customerSearch = customerSearch;
    }

    private List<String> customerSearch;
    private List<String> customerType;
    private List<String> addressType;


    private List<Customer> customerList;
    private Customer customer1;
    private Customer customer2;
    private Customer customer3;
    private Customer customer4;
    private Customer customer5;
    private Customer customer6;
    private Customer customer7;
    private Customer customer8;
    private Customer customer9;
    private Customer customer10;

    public String execute() throws Exception {
        customerList = new ArrayList<Customer>();

        customer1 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");
        customer2 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");
        customer3 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");
        customer4 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");
        customer5 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");
        customer6 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");
        customer7 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");
        customer8 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");
        customer9 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");
        customer10 = new Customer(1, "SRI", "Solutions Resource", "info@solutionsresource.com", "123-4567");

        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);
        customerList.add(customer5);
        customerList.add(customer6);
        customerList.add(customer7);
        customerList.add(customer8);
        customerList.add(customer9);
        customerList.add(customer10);

        customerSearch = new ArrayList<String>();
        customerSearch.add("Company Name");
        customerSearch.add("Company Code");
        customerSearch.add("Customer Type");
        customerSearch.add("Email Address");
        customerSearch.add("Contact Number");

        customerType = new ArrayList<String>();
        customerType.add("A");
        customerType.add("B");
        customerType.add("C");
        customerType.add("D");

        addressType = new ArrayList<String>();

        addressType.add("BRANCH 1");
        addressType.add("BRANCH 2");
        addressType.add("BRANCH 3");
        addressType.add("ANNEX 1");
        addressType.add("ANNEX 2");
        addressType.add("ANNEX 3");
        addressType.add("HOME");
        addressType.add("MAIN");

        return SUCCESS;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }
    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public List<String> getCustomerType() { return customerType; }
    public void setCustomerType(List<String> customerType) { this.customerType = customerType; }

    public List<String> getAddressType() { return addressType; }
    public void setAddressType(List<String> addressType) { this.addressType = addressType; }

}
