package com.sri.janernest.action;

/**
 * Created by Solutions Resource on 4/11/14.
 */

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

public class EditPermissionAction extends ActionSupport {



    private List<String> group;
    private List<String> user;

    public String execute() throws Exception {


        return SUCCESS;
    }

    public EditPermissionAction(){


        group = new ArrayList<String>();
        user = new ArrayList<String>();

        group.add("Admin");
        group.add("CRD");
        group.add("Planning");
        group.add("Customer");

        user.add("Jan");
        user.add("User1");
        user.add("User2");
        user.add("User3");

    }
    public String display() {
        return NONE;
    }

    public List<String> getUser() {
        return user;
    }
    public void setUser(List<String> user) {
        this.user = user;
    }
    public List<String> getGroup() {
        return group;
    }
    public void setGroup(List<String> group) {
        this.group = group;
    }
}
