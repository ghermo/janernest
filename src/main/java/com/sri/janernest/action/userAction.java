package com.sri.janernest.action;

/**
 * Created by JMXPSX on 3/27/14.
 */

import com.sri.janernest.model.user;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.List;

public class userAction extends ActionSupport {

    private static final long serialVersionUID = 1L;

   private List<String> userType;

    private List<user> users;
    private user user1;
    private user user2;
    private user user3;
    private user user4;
    private user user5;

    public String execute() throws Exception {

        users = new ArrayList<user>();

        user1 = new user(1,"a1","a1","a1","a1","a1","a1","a1","a1","a1");
        user2 = new user(2,"Jet Castro","Solutions Resource","JET","srllc00","CEO","jet@solutionsresource.com","9488704","Admin","Active");
        user3 = new user(3,"c3","c3","c3","c3","a1","a1","a1","a1","a1");
        user4 = new user(4,"d4","d4","d4","d4","a1","a1","a1","a1","a1");
        user5 = new user(5,"e5","e5","e5","e5","a1","a1","a1","a1","a1");

        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);

        userType = new ArrayList<String>();
        userType.add("Registered User");
        userType.add("Customer Relations Department");
        userType.add("Freight Operations Officer");
        userType.add("Freight Operations Specialist");
        userType.add("Freight Operations Manager");
        userType.add("Freight Documents Specialist");
        userType.add("Inland Freight Officer");

        return SUCCESS;

    }

    public List<user> getUsers() {
        return users;
    }

    public void setUsers(List<user> users) {
        this.users = users;
    }

    public List<String> getUserType() {
        return userType;
    }

    public void setUserType(List<String> userType) {
        this.userType = userType;
    }
}
