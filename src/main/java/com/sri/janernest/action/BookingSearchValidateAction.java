package com.sri.janernest.action;


import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by JMXPSX on 4/25/14.
 */
public class BookingSearchValidateAction extends ActionSupport{

    private String keyword;

    private List<String> status;

    public void validate(){

        status = new ArrayList<String>();
        status.add("Booking Number");
        status.add("Shipper");
        status.add("Company Name");
        status.add("Contact Person");
        status.add("Contact Number");
        status.add("Address");
        status.add("City");

    if (keyword == null || keyword.trim().equals("")){
            addFieldError("keyword", "Keyword should not be empty");
        }

    }

    public String execute() throws Exception{
        return SUCCESS;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<String> getStatus() {
        return status;
    }

    public void setStatus(List<String> status) {
        this.status = status;
    }
}
