package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clarence Victoria on 4/14/14.
 */
public class VendorValidateAddVendor extends ActionSupport{


    private String companyType;
    private String companyName;
    private String companyCode;

    private List<String> vendorTypeList;
    private List<String> truckTypeList;

    public void validate(){

        if (companyType == null || companyType.trim().equals("---")){
            addFieldError("companyType", "Company Type must not be filled out");
        }
        if (companyName == null || companyName.trim().equals("")){
            addFieldError("companyName", "Company Name must not be filled out");
        }
        if (companyCode == null || companyCode.trim().equals("")){
            addFieldError("companyCode", "Company Code must not be filled out");
        }

        vendorTypeList = new ArrayList<String>();
        vendorTypeList.add("---");
        vendorTypeList.add("Trucking");
        vendorTypeList.add("Shipping");

        truckTypeList = new ArrayList<String>();
        truckTypeList.add("Tractor Head");
        truckTypeList.add("Closed Van");
    }

    public String execute(){
        return SUCCESS;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public List getVendorTypeList() {
        return vendorTypeList;
    }

    public List<String> getTruckTypeList() {
        return truckTypeList;
    }

    public void setTruckTypeList(List<String> truckTypeList) {
        this.truckTypeList = truckTypeList;
    }

    public void setVendorTypeList(List<String> vendorTypeList) {
        this.vendorTypeList = vendorTypeList;
    }
}
