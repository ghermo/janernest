package com.sri.janernest.action;

/**
 * Created by Jimmy on 4/1/14.
 */


import com.opensymphony.xwork2.ActionSupport;

public class FinanceEditValidation extends ActionSupport {

    private static final long serialVersionUID = 1L;

    public String name;
    public String username;
    public String company;
    public String email;
    public String contact;

        public String execute(){

            return SUCCESS;
    }
    public void validate(){

        if (username == null || username.trim().equals("")){
            addFieldError("username","This field could not be empty");
        }
        if (name == null || name.trim().equals("")){
            addFieldError("name","This field could not be empty");
        }
        if (company == null || company.trim().equals("")){
            addFieldError("company","This field could not be empty");
        }
        if (email == null || email.trim().equals("")){
            addFieldError("email","This field could not be empty");
        }
        if (contact == null || contact.trim().equals("")){
            addFieldError("contact","This field could not be empty");
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
