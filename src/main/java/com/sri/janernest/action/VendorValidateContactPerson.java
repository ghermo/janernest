package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clarence Victoria on 4/16/14.
 */
public class VendorValidateContactPerson extends ActionSupport {

    private List<String> statusList;
    private List<String> otherInfoClassList;

    private String firstName;
    private String middleName;
    private String lastName;
    private String number;

    public void validate() {

        if (firstName == null || firstName.trim().equals("")){
            addFieldError("firstName","Null");
        }
        if (middleName == null || middleName.trim().equals("")){
            addFieldError("middleName","Null");
        }
        if (lastName == null || lastName.trim().equals("")){
            addFieldError("lastName","Null");
        }
        if (number == null || number.trim().equals("")){
            addFieldError("number","Null");
        }

        statusList = new ArrayList<String>();
        statusList.add("Active");
        statusList.add("Inactive");

        otherInfoClassList = new ArrayList<String>();
        otherInfoClassList.add("---");
        otherInfoClassList.add("Premium");
        otherInfoClassList.add("Class A");
        otherInfoClassList.add("Class B");
        otherInfoClassList.add("Class C");
        otherInfoClassList.add("Economy");
    }

    public String execute() throws Exception{
        return SUCCESS;
    }

    public List<String> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<String> statusList) {
        this.statusList = statusList;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<String> getOtherInfoClassList() {
        return otherInfoClassList;
    }

    public void setOtherInfoClassList(List<String> otherInfoClassList) {
        this.otherInfoClassList = otherInfoClassList;
    }
}
