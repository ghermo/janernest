package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clarence Victoria on 4/16/14.
 */
public class VendorValidateVessel extends ActionSupport {

    private List<String> otherInfoClassList;
    private List<String> statusList;

    private String number;
    private String name;
    private String modelNumber;
    private String yearModel;

    public void validate(){

        if (number == null || number.trim().equals("")){
            addFieldError("number","Null");
        }
        if (name == null || name.trim().equals("")){
            addFieldError("name","Null");
        }
        if (modelNumber == null || modelNumber.trim().equals("")){
            addFieldError("modelNumber","Null");
        }
        if (yearModel == null || yearModel.trim().equals("")){
            addFieldError("yearModel","Null");
        }
        otherInfoClassList = new ArrayList<String>();
        otherInfoClassList.add("---");
        otherInfoClassList.add("Premium");
        otherInfoClassList.add("Class A");
        otherInfoClassList.add("Class B");
        otherInfoClassList.add("Class C");
        otherInfoClassList.add("Economy");

        statusList = new ArrayList<String>();
        statusList.add("Active");
        statusList.add("Inactive");
    }

    public String execute() throws  Exception{
        return SUCCESS;
    }

    public List<String> getOtherInfoClassList() {
        return otherInfoClassList;
    }

    public void setOtherInfoClassList(List<String> otherInfoClassList) {
        this.otherInfoClassList = otherInfoClassList;
    }

    public List<String> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<String> statusList) {
        this.statusList = statusList;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getYearModel() {
        return yearModel;
    }

    public void setYearModel(String yearModel) {
        this.yearModel = yearModel;
    }
}
