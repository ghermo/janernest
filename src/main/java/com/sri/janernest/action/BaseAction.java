package com.sri.janernest.action;


import com.opensymphony.xwork2.ActionSupport;
/**
 * Created by grg021 on 3/28/14.
 */
public class BaseAction extends ActionSupport {

    private static final long serialVersionUID = -2613425890762568273L;

    public String home()
    {
        return "home";
    }

    public String customer()
    {
        return "customer";
    }

    public String vendor()
    {
        return "vendor";
    }

    public String booking()
    {
        return "booking";
    }
}
