package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clarence Victoria on 4/16/14.
 */
public class VendorValidateTrucks extends ActionSupport{

    private List<String> truckTypeList;
    private List<String> statusList;

    private String truckType;
    private String plateNumber;
    private String yearModel;
    private String engineNumber;
    private String modelNumber;

    public void validate(){
        if (truckType == null || truckType.trim().equals("")){
            addFieldError("truckType", "Null");
        }
        if (plateNumber == null || plateNumber.trim().equals("")){
            addFieldError("plateNumber", "Null");
        }
        if (yearModel == null || yearModel.trim().equals("")){
            addFieldError("yearModel", "Null");
        }
        if (engineNumber == null || engineNumber.trim().equals("")){
            addFieldError("engineNumber", "Null");
        }
        if (modelNumber == null || modelNumber.trim().equals("")){
            addFieldError("modelNumber", "Null");
        }

        truckTypeList = new ArrayList<String>();
        truckTypeList.add("Tractor Head");
        truckTypeList.add("Closed Van");

        statusList = new ArrayList<String>();
        statusList.add("Active");
        statusList.add("Inactive");
    }

    public String execute() throws Exception{
        return SUCCESS;
    }

    public List<String> getTruckTypeList() {
        return truckTypeList;
    }

    public void setTruckTypeList(List<String> truckTypeList) {
        this.truckTypeList = truckTypeList;
    }

    public String getTruckType() {
        return truckType;
    }

    public void setTruckType(String truckType) {
        this.truckType = truckType;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getYearModel() {
        return yearModel;
    }

    public void setYearModel(String yearModel) {
        this.yearModel = yearModel;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public List<String> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<String> statusList) {
        this.statusList = statusList;
    }
}
