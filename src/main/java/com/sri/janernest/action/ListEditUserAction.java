package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by JMXPSX on 4/25/14.
 */
public class ListEditUserAction extends ActionSupport{

    public List<String> userType;
    public String uName;
    public String uCompanyName;
    public String uUserName;
    public String uPassword;
    public String uTitle;
    public String uEmailAddress;
    public String uContactNumber;
    public String uStatus;

    public String execute() {

        return SUCCESS;
    }

    public ListEditUserAction() {

        userType = new ArrayList<String>();
        userType.add("Registered User");
        userType.add("Customer Relations Department");
        userType.add("Freight Operations Officer");
        userType.add("Freight Operations Specialist");
        userType.add("Freight Operations Manager");
        userType.add("Freight Documents Specialist");
        userType.add("Inland Freight Officer");
    }

    public void validate() {
        if(uCompanyName == null || uCompanyName.trim().equals("")){
            addFieldError("uCompanyName","Company Name is required");
        }
        if(uName == null || uName.trim().equals("")){
            addFieldError("uName","Name is required");
        }
        if(uUserName == null || uUserName.trim().equals("")){
            addFieldError("uUserName","Username is required");
        }
        if(uPassword == null || uPassword.trim().equals("")){
            addFieldError("uPassword","Password is required");
        }
        if(uTitle == null || uTitle.trim().equals("")){
            addFieldError("uTitle","Title is required");
        }
        if(uEmailAddress == null || uEmailAddress.trim().equals("")){
            addFieldError("uEmailAddress","Email Address is required");
        }
        if(uContactNumber == null || uContactNumber.trim().equals("")){
            addFieldError("uContactNumber","Contact Number is required");
        }
        if(uStatus == null || uStatus.trim().equals("")){
            addFieldError("uStatus","Status is required" );
        }

    }

    public List<String> getUserType() {
        return userType;
    }

    public void setUserType(List<String> userType) {
        this.userType = userType;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuCompanyName() {
        return uCompanyName;
    }

    public void setuCompanyName(String uCompanyName) {
        this.uCompanyName = uCompanyName;
    }

    public String getuUserName() {
        return uUserName;
    }

    public void setuUserName(String uUserName) {
        this.uUserName = uUserName;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getuTitle() {
        return uTitle;
    }

    public void setuTitle(String uTitle) {
        this.uTitle = uTitle;
    }

    public String getuEmailAddress() {
        return uEmailAddress;
    }

    public void setuEmailAddress(String uEmailAddress) {
        this.uEmailAddress = uEmailAddress;
    }

    public String getuContactNumber() {
        return uContactNumber;
    }

    public void setuContactNumber(String uContactNumber) {
        this.uContactNumber = uContactNumber;
    }

    public String getuStatus() {
        return uStatus;
    }

    public void setuStatus(String uStatus) {
        this.uStatus = uStatus;
    }
}
