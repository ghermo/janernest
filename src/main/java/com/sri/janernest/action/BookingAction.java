package com.sri.janernest.action;

/**
 * Created by grg021 on 3/28/14.
 */

import com.sri.janernest.model.Booking;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookingAction extends ActionSupport {

    private static final long serialVersionUID = 1L;

    private List<String> status;

    private List<String> serviceReqt;

    private List<String> serviceMode;

    private List<String> paymentMode;

    private List<String> containerQty;

    private List<String> containerSize;

    private Date pickupDate;

    private List<Booking> bookings;
    private Booking book1;
    private Booking book2;
    private Booking book3;
    private Booking book4;
    private Booking book5;
    private Date d;

    public Date getTodayDate(){

        return new Date();
    }

    public String execute() throws Exception {

        bookings = new ArrayList<Booking>();
        d = new Date();

        book1 = new Booking(1, d, 1, "b1", "b1", "b1", "1");
        book2 = new Booking(2, d, 2, "c1", "c1", "c1", "1");
        book3 = new Booking(3, d, 3, "d1", "d1", "d1", "1");
        book4 = new Booking(4, d, 4, "e1", "e1", "e1", "4");
        book5 = new Booking(5, d, 5, "f1", "f1", "f1", "5");

        bookings.add(book1);
        bookings.add(book2);
        bookings.add(book3);
        bookings.add(book4);
        bookings.add(book5);

        status = new ArrayList<String>();
        status.add("Booking Number");
        status.add("Shipper");
        status.add("Company Name");
        status.add("Contact Person");
        status.add("Contact Number");
        status.add("Address");
        status.add("City");

        serviceReqt = new ArrayList<String>();
        serviceReqt.add("Full Cargo Load");
        serviceReqt.add("Less Cargo Load");
        serviceReqt.add("Rolling Cargo");

        serviceMode = new ArrayList<String>();
        serviceMode.add("Door to Door");
        serviceMode.add("Door to Pier");
        serviceMode.add("Pier to Door");
        serviceMode.add("Pier to Pier");

        paymentMode = new ArrayList<String>();
        paymentMode.add("Freight Prepaid");
        paymentMode.add("Freight Collect");
        paymentMode.add("Account Destination");
        paymentMode.add("Account Origin");

        containerQty = new ArrayList<String>();
        containerQty.add("1");
        containerQty.add("2");
        containerQty.add("3");
        containerQty.add("4");
        containerQty.add("5");

        containerSize = new ArrayList<String>();
        containerSize.add("10 Footer");
        containerSize.add("20 Footer");
        containerSize.add("40 Footer");

        return SUCCESS;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }

    public List<String> getStatus() {
        return status;
    }

    public void setStatus(List<String> status) {
        this.status = status;
    }

    public List<String> getServiceReqt() {
        return serviceReqt;
    }

    public void setServiceReqt(List<String> serviceReqt) {
        this.serviceReqt = serviceReqt;
    }

    public List<String> getServiceMode() {
        return serviceMode;
    }

    public void setServiceMode(List<String> serviceMode) {
        this.serviceMode = serviceMode;
    }

    public List<String> getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(List<String> paymentMode) {
        this.paymentMode = paymentMode;
    }

    public List<String> getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(List<String> containerQty) {
        this.containerQty = containerQty;
    }

    public List<String> getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(List<String> containerSize) {
        this.containerSize = containerSize;
    }

    public Date getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
    }
}
