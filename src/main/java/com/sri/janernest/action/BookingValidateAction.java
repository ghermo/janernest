package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JMXPSX on 4/16/14.
 */
public class BookingValidateAction extends ActionSupport{

    private List<String> serviceReqt;

    private List<String> serviceMode;

    private List<String> paymentMode;

    private List<String> containerQty;

    private List<String> containerSize;

    private String bookingNumber;
    private String shipperName;
    private String shipperPerson;
    private String shipperNumber;
    private String shipperAddress;
    private String originPort;
    private String pickupAddress;
    private String consigneeName;
    private String consigneePerson;
    private String consigneeNumber;
    private String consigneeAddress;
    private String destinationPort;
    private String deliveryAddress;
    private String cargoClass;
    private String estWeight;
    private String declaredValue;
    private String commDescription;
    private String remarks;
    private String rate;

    public String execute() {
        return SUCCESS;
    }

    public BookingValidateAction() {

        serviceReqt = new ArrayList<String>();
        serviceReqt.add("Full Cargo Load");
        serviceReqt.add("Less Cargo Load");
        serviceReqt.add("Rolling Cargo");

        serviceMode = new ArrayList<String>();
        serviceMode.add("Door to Door");
        serviceMode.add("Door to Pier");
        serviceMode.add("Pier to Door");
        serviceMode.add("Pier to Pier");

        paymentMode = new ArrayList<String>();
        paymentMode.add("Freight Prepaid");
        paymentMode.add("Freight Collect");
        paymentMode.add("Account Destination");
        paymentMode.add("Account Origin");

        containerQty = new ArrayList<String>();
        containerQty.add("1");
        containerQty.add("2");
        containerQty.add("3");
        containerQty.add("4");
        containerQty.add("5");

        containerSize = new ArrayList<String>();
        containerSize.add("10 Footer");
        containerSize.add("20 Footer");
        containerSize.add("40 Footer");

    }

    public void validate() {

        if (bookingNumber == null || bookingNumber.trim().equals("")){
            addFieldError("bookingNumber","Booking Number must be filled out");
        }
        if (shipperName == null || shipperName.trim().equals("")){
            addFieldError("shipperName","Shipper must be filled out");
        }
        if (shipperPerson == null || shipperPerson.trim().equals("")){
            addFieldError("shipperPerson","Contact Person must be filled out");
        }
        if (shipperNumber == null || shipperNumber.trim().equals("")){
            addFieldError("shipperNumber","Contact Number must be filled out");
        }
        if (shipperAddress == null || shipperAddress.trim().equals("")){
            addFieldError("shipperAddress","Address must be filled out");
        }
        if (originPort == null || originPort.trim().equals("")){
            addFieldError("originPort","Port of Origin must be filled out");
        }
        if (pickupAddress == null || pickupAddress.trim().equals("")){
            addFieldError("pickupAddress","Pick-up Address must be filled out");
        }
        if (consigneeName == null || consigneeName.trim().equals("")){
            addFieldError("consigneeName","Consignee must be filled out");
        }
        if (consigneePerson == null || consigneePerson.trim().equals("")){
            addFieldError("consigneePerson","Contact Person must be filled out");
        }
        if (consigneeNumber == null || consigneeNumber.trim().equals("")){
            addFieldError("consigneeNumber","Contact Number must be filled out");
        }
        if (consigneeAddress == null || consigneeAddress.trim().equals("")){
            addFieldError("consigneeAddress","Address must be filled out");
        }
        if (destinationPort == null || destinationPort.trim().equals("")){
            addFieldError("destinationPort","Port of Destination must be filled out");
        }
        if (deliveryAddress == null || deliveryAddress.trim().equals("")){
            addFieldError("deliveryAddress","Delivery Address must be filled out");
        }
        if (cargoClass == null || cargoClass.trim().equals("")){
            addFieldError("cargoClass","Cargo Classification must be filled out");
        }
        if (estWeight == null || estWeight.trim().equals("")){
            addFieldError("estWeight","Estimated Weight must be filled out");
        }
        if (declaredValue == null || declaredValue.trim().equals("")){
            addFieldError("declaredValue","Declared Value must be filled out");
        }
        if (commDescription == null || commDescription.trim().equals("")){
            addFieldError("commDescription","Commodity Description must be filled out");
        }
        if (remarks == null || remarks.trim().equals("")){
            addFieldError("remarks","Remarks must be filled out");
        }
        if (rate == null || rate.trim().equals("")){
            addFieldError("rate","Rate must be filled out");
        }

    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getOriginPort() {
        return originPort;
    }

    public void setOriginPort(String originPort) {
        this.originPort = originPort;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public String getShipperPerson() {
        return shipperPerson;
    }

    public void setShipperPerson(String shipperPerson) {
        this.shipperPerson = shipperPerson;
    }

    public String getShipperNumber() {
        return shipperNumber;
    }

    public void setShipperNumber(String shipperNumber) {
        this.shipperNumber = shipperNumber;
    }

    public String getShipperAddress() {
        return shipperAddress;
    }

    public void setShipperAddress(String shipperAddress) {
        this.shipperAddress = shipperAddress;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneePerson() {
        return consigneePerson;
    }

    public void setConsigneePerson(String consigneePerson) {
        this.consigneePerson = consigneePerson;
    }

    public String getConsigneeNumber() {
        return consigneeNumber;
    }

    public void setConsigneeNumber(String consigneeNumber) {
        this.consigneeNumber = consigneeNumber;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getDestinationPort() {
        return destinationPort;
    }

    public void setDestinationPort(String destinationPort) {
        this.destinationPort = destinationPort;
    }

    public String getCargoClass() {
        return cargoClass;
    }

    public void setCargoClass(String cargoClass) {
        this.cargoClass = cargoClass;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getEstWeight() {
        return estWeight;
    }

    public void setEstWeight(String estWeight) {
        this.estWeight = estWeight;
    }

    public String getDeclaredValue() {
        return declaredValue;
    }

    public void setDeclaredValue(String declaredValue) {
        this.declaredValue = declaredValue;
    }

    public String getCommDescription() {
        return commDescription;
    }

    public void setCommDescription(String commDescription) {
        this.commDescription = commDescription;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public List<String> getServiceReqt() {
        return serviceReqt;
    }

    public void setServiceReqt(List<String> serviceReqt) {
        this.serviceReqt = serviceReqt;
    }

    public List<String> getServiceMode() {
        return serviceMode;
    }

    public void setServiceMode(List<String> serviceMode) {
        this.serviceMode = serviceMode;
    }

    public List<String> getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(List<String> paymentMode) {
        this.paymentMode = paymentMode;
    }

    public List<String> getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(List<String> containerQty) {
        this.containerQty = containerQty;
    }

    public List<String> getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(List<String> containerSize) {
        this.containerSize = containerSize;
    }


}
