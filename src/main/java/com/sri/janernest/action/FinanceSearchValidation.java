package com.sri.janernest.action;

/**
 * Created by Jimmy on 4/1/14.
 */


import com.opensymphony.xwork2.ActionSupport;

public class FinanceSearchValidation extends ActionSupport {

    private static final long serialVersionUID = 1L;

    public String username;

        public String execute(){

            return SUCCESS;
    }
    public void validate(){

        if (username == null || username.trim().equals("")){
            addFieldError("username","Please enter a username");
        }

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
