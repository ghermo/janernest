package com.sri.janernest.action;

/**
 * Created by grg021 on 3/27/14.
 */

import com.sri.janernest.model.MessageStore;
import com.opensymphony.xwork2.ActionSupport;

public class HelloWorldAction extends ActionSupport {

    private static final long serialVersionUID = 1L;

    private MessageStore messageStore;

    public String execute() throws Exception {

        messageStore = new MessageStore() ;
        return SUCCESS;
    }

    public String executereport() throws Exception {

        messageStore = new MessageStore() ;
        return SUCCESS;
    }


    public MessageStore getMessageStore() {
        return messageStore;
    }

    public void setMessageStore(MessageStore messageStore) {
        this.messageStore = messageStore;
    }

}