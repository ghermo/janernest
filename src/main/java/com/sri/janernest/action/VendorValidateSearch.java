package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clarence Victoria on 4/23/14.
 */
public class VendorValidateSearch extends ActionSupport {

    private String search;
    private String keyword;

    private List<String> searchByList;

    public void validate(){

        searchByList = new ArrayList<String>();
        searchByList.add("Company Name");
        searchByList.add("Company Code");
        searchByList.add("Vendor Type");
        searchByList.add("Email Address");
        searchByList.add("Contact Number");
        searchByList.add("Class");
        searchByList.add("City");

        if (search == null || search.trim().equals("")){
            addFieldError("search", "NULL");
        }

        if (keyword == null || keyword.trim().equals("")){
            addFieldError("keyword", "NULL");
        }
    }

    public String execute() throws Exception{
        return SUCCESS;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List getSearchByList() {
        return searchByList;
    }

    public void setSearchByList(List searchByList) {
        this.searchByList = searchByList;
    }
}
