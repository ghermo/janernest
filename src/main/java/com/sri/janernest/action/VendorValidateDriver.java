package com.sri.janernest.action;

import com.opensymphony.xwork2.ActionSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Clarence Victoria on 4/16/14.
 */
public class VendorValidateDriver extends ActionSupport {

    private String licenseNumber;
    private String firstName;
    private String middleName;
    private String lastName;
    private String status;

    private List<String> statusList;

    public void validate(){

        statusList = new ArrayList<String>();
        statusList.add("Active");
        statusList.add("Inactive");

        if (licenseNumber == null || licenseNumber.trim().equals("")){
            addFieldError("licenseNumber", "Null");
        }
        if (firstName == null || firstName.trim().equals("")){
            addFieldError("firstName", "Null");
        }
        if (middleName == null || middleName.trim().equals("")){
            addFieldError("middleName", "Null");
        }
        if (lastName == null || lastName.trim().equals("")){
            addFieldError("lastName", "Null");
        }
        if (status == null || status.trim().equals("")){
            addFieldError("status", "Null");
        }
    }

    public String execute() throws Exception{
        return SUCCESS;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<String> statusList) {
        this.statusList = statusList;
    }
}
