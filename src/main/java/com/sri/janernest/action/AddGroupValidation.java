package com.sri.janernest.action;

/**
 * Created by ADMIN on 4/3/14.
 */
import com.opensymphony.xwork2.ActionSupport;
public class AddGroupValidation extends ActionSupport {

    public String code;
    public String name;
    public String desc;

    public String execute()
    {

        return SUCCESS;
    }


        public void validate(){

            if(code == null || code.trim().equals("")){
                addFieldError("code","The code is required");
            }
            if(name == null || name.trim().equals("")){
                addFieldError("name","The name is required");
            }
            if(desc == null || desc.trim().equals("")){
                addFieldError("desc","The Description is required");
            }



        }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }



}
