<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>


<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

		<h1 class="page-header">Users</h1>

		<!-- EDIT HERE -->

		<div class="row">
				<div class="col-md-12">
				  <div class="panel panel-info">

					  <div class="panel-heading">

					  	<img src="images/listofusers.png" class="box-icon" />
						<span class="panel-title">List of Users</span>
						</div>
						<div class="panel-body">
						<div class="table-responsive list-table">
							<table class="table table-striped table-bordered text-center" id ="users-list">
							  <thead>
								<tr class="header_center">
								  <th class="tb-font-black"><input type="checkbox" /></th>
								  <th class="tb-font-black">User Code</th>
								  <th class="tb-font-black">Name</th>
								  <th class="tb-font-black">Company Name</th>
								  <th class="tb-font-black">User Name</th>
								  <th class="tb-font-black">User Type</th>
								  <th class="tb-font-black">Action</th>
								</tr>
							  </thead>
							  <tbody>
							    <s:iterator value="users" var="user">
								<tr>
								  <td class="tb-font-black"><input type="checkbox" /></td>
								  <td class="tb-font-black"><s:property value="#user.userCode"/></td>
								  <td class="tb-font-black" align="center"><s:property value="#user.name"/></td>
								  <td class="tb-font-black"><s:property value="#user.companyName"/></td>
								  <td class="tb-font-black"><s:property value="#user.userName"/></td>
								  <td class="tb-font-black"><s:property value="#user.userType"/></td>
								  <td class="tb-font-black">
									<a href="User-List-Edit" class="icon-action-link" rel="tooltip" title="Edit this user"><img src="images/edit-user.png" class="icon-action circ-icon"> </a>
									<a href="#delete-booking" data-toggle="modal" data-controls-modal="#delete-booking" data-backdrop="static" data-keyboard="false" class="icon-action-link" rel="tooltip" title="Delete this user"><img src="images/remove-user.png" class="icon-action circ-icon"> </a>
									<a href="User-Info" class="icon-action-link" rel="tooltip" title="View this user"><img src="images/info-b.png" class="icon-action circ-icon"> </a>
								  </td>
								</tr>
								</s:iterator>
							  </tbody>
							</table>
						  </div>
					  </div>


							<div class="panel-footer">
							<span class="pull-right">
								<a href="User-Add" class="icon-action-link" rel="tooltip" title="Add User"><img src="images/add-user.png" class="icon-action circ-icon"> </a>
							</span>

							<ul class="pagination">
							  <li class="disabled"><a href="#">&laquo;</a></li>
							  <li class="active"><a href="#">1</a></li>
							  <li class="disabled"><a href="#">&raquo;</a></li>
							</ul>

					  </div>
					  </div>

				  </div>
			    </div>
			</div>

		</div>


		<div class="modal fade" id="delete-booking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        	  <div class="modal-dialog">
        		<div class="modal-content">
        		  <div class="modal-header">
        			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        			<h4 class="modal-title" id="myModalLabel"> <img src="images/delete-booking-b.png" /> &nbsp; Confirm Delete</h4>
        		  </div>
        		  <div class="modal-body">
        			<div style="text-align:center;">
        					<span>Do you really want to delete <em>'001, Russel, Company 001, Russelpo, Freight Operations Specialist'</em> ? This action is irreversible.</span>
        			</div>
        		  </div>
        		  <div class="modal-footer">
        			<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        			<button type="button" class="btn btn-primary">Yes</button>
        		  </div>
        		</div>
        	  </div>
        	</div>