<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
        
		<!-- MIDDLE -->
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
		<h1 class="page-header">Users</h1>
		
		
		<!-- EDIT HERE -->
		<div class="row alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  			<strong>Success!</strong> The data has been successfully saved.
		</div>

		<div class="row">			
			<div class="col-md-12">
				<div class="panel panel-info">
						
						<div class="panel-heading">
					  	<img src="images/listofusers.png" class="box-icon" />
						<span class="panel-title">Edit User</h3>
						</div>
						
					<div class="panel-body">
						

							<s:form action="User-Save-Complete" theme="bootstrap" cssClass="form-horizontal" >
							<div class="form-group">

							    <label for="uType" class="col-sm-2 control-label">User Type:</label>

							    <div class="col-sm-10">
							        <s:select name="uType" id="uType" list="userType" cssClass="form-control" />
							    </div>

							  </div>
						<div class="form-group">

							    <label for="uName" class="col-sm-2 control-label">Name:</label>

							    <div class="col-sm-10">
							      <s:textfield cssClass="form-control" id="uName" name="uName" placeholder="Name"></s:textfield>
							    </div>

							  </div>		    
							  
							<div class="form-group">
							
							<label for="uCompanyName" class="col-sm-2 control-label">Company:</label>

							    <div class="col-sm-10">
							      <s:textfield cssClass="form-control" id="uCompanyName" name="uCompanyName" placeholder="Company Name"></s:textfield>
							    </div>
							</div>

							  <div class="form-group">

							    <label for="uUserName" class="col-sm-2 control-label">User Name:</label>

							    <div class="col-sm-10">
							      <s:textfield cssClass="form-control" id="uUserName" name="uUserName" placeholder="User Name"></s:textfield>
							    </div>

							  </div>
							  
							   <div class="form-group">

							    <label for="uPassword" class="col-sm-2 control-label">Password:</label>

							    <div class="col-sm-10">
							      <s:textfield cssClass="form-control" id="uPassword" name="uPassword" placeholder="Password"></s:textfield>
							    </div>

							  </div>
							  
							   <div class="form-group">

							    <label for="uTitle" class="col-sm-2 control-label">Title:</label>

							    <div class="col-sm-10">
							      <s:textfield cssClass="form-control" id="uTitle" name="uTitle" placeholder="Title"></s:textfield>
							    </div>

							  </div>
							  
							   <div class="form-group">

							    <label for="uEmailAddress" class="col-sm-2 control-label">Email Address:</label>

							    <div class="col-sm-10">
							      <s:textfield cssClass="form-control" id="uEmailAddress" name="uEmailAddress" placeholder="Email Address"></s:textfield>
							    </div>

							  </div>
							  
							   <div class="form-group">

							    <label for="uContactNumber" class="col-sm-2 control-label">Contact Number:</label>

							    <div class="col-sm-10">
							      <s:textfield cssClass="form-control" id="uContactNumber" name="uContactNumber" placeholder="Contact Number"></s:textfield>
							    </div>

							  </div>
							  
							   <div class="form-group">

							    <label for="uStatus" class="col-sm-2 control-label">Status:</label>

							    <div class="col-sm-10">
							      <s:textfield cssClass="form-control" id="uStatus" name="uStatus" placeholder="Status"></s:textfield>
							    </div>

							  </div>



						</div>

					</div>
						
						<div class="btn-group" style="float: right;">
						<button class="btn btn-default" onclick="location.href='User-Save-Complete'">Save</button>
						<button class="btn btn-default" onclick="location.href='User-List.html'">Cancel</button>
						</div>
						</s:form>
				</div>
				
			</div>
			


		<!--<div id="openModal" class="modalDialog">
			<div>
				<div class="panel panel-info">
					<a href="#close" title="Close" class="close">X</a>

					<div class="panel-heading">
						<h3 class="panel-title">Confirm Delete</h3>
					</div>

					<div class="panel-body">
						<div style="text-align:center;">
							<span>Do you really want to delete <em>'001, Russel, Company 001, Russelpo, Freight Operations Specialist'</em> ? This action is irreversible.</span>
						</div>
					</div>

					<div class="panel-footer">
						<div class="btn-group btn-group-justified">
							<div class="btn-group">
								<button class="btn btn-default" onclick="location.href='#close'">Yes</button>
							</div>
							<div class="btn-group">
								<button class="btn btn-default" onclick="location.href='#close'">No</button>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>-->

		<!-- SIDEBAR GOES HERE -->
		
		
		<!-- END OF EDIT -->
		
        
		
		<!-- END OF THE MIDDLE -->
